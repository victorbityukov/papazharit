import React, {Component} from 'react';
import './App.css';
import HeaderContainer from "./components/Header/Header";
import {BrowserRouter} from "react-router-dom";
import FooterContainer from "./components/Footer/Footer";
import Content from "./components/Content/Content";
import {Provider} from "react-redux";
import ReactPixel from "react-facebook-pixel";

class App extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    ReactPixel.init('2675202449403331');
    ReactPixel.pageView();
  }

  render() {
    return (
      <div className="App">
        <Provider store={this.props.store}>
          <BrowserRouter>
            <HeaderContainer/>
            <Content/>
            <FooterContainer/>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
}

export default App;
