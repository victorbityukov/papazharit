const SET_PRODUCTS = 'SET_PRODUCTS';

let initialState = {
  isReady: false,
  items: null
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        ...state,
        isReady: true,
        items: action.products
      };
    default:
      return state;
  }
};

export const setProducts = (products) => (
  {
    type: SET_PRODUCTS,
    products
  });

export default productsReducer;