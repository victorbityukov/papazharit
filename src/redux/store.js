import {combineReducers, createStore} from "redux";

import productsReducer from "./products-reducer";
import basketReducer from "./basket-reducer";
import interfaceReducer from "./interface-reducer";

let reducers = combineReducers({
  products: productsReducer,
  basket: basketReducer,
  inter: interfaceReducer
});

let store = createStore(reducers);

export default store;