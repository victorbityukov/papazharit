import {isEmpty} from "lodash";
import ReactPixel from "react-facebook-pixel";

const ADD_TO_BASKET = 'ADD_TO_BASKET';
const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET';
const SET_EMPTY_BASKET = 'SET_EMPTY_BASKET';
const CHECK_BASKET = 'CHECK_BASKET';

let initialState = {
  items: {},
  infoForOrder: {
    items: {},
    idOrder: ''
  },
  isCheck: false
};

const basketReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_BASKET:
      ReactPixel.track('AddToCart');
      let count = 1;
      if (state.items[action.item.id]) {
        count += state.items[action.item.id].count
      }
      let newItem = {...action.item, count};
      let newStateAdd = {
        ...state,
        items:
          {
            ...state.items,
            [action.item.id]: newItem,
          }
      };
      localStorage.setItem('basketPZ', JSON.stringify(newStateAdd));
      return newStateAdd;
    case REMOVE_FROM_BASKET:
      let newStateItems = {
        ...state.items
      };
      let newCount = state.items[action.id].count - 1;

      if (newCount <= 0) {
        delete newStateItems[action.id]
      } else {
        newStateItems[action.id].count = newCount;
      }
      let newStateRem = {
        ...state,
        items: newStateItems
      };
      localStorage.setItem('basketPZ', JSON.stringify(newStateRem));
      return newStateRem;
    case CHECK_BASKET:
      let stateCheck = {...state};
      if (!isEmpty(localStorage.getItem('basketPZ'))) {
        stateCheck = JSON.parse(localStorage.getItem('basketPZ'));
      }
      return {
        ...stateCheck,
        isCheck: true
      };
    case SET_EMPTY_BASKET:
      localStorage.clear();
      return {
        ...state,
        infoForOrder: {
          items: state.items,
          idOrder: action.idOrder
        },
        items: {},
      };
    default:
      return state;
  }
};

export const addToBasket = item => (
  {
    type: ADD_TO_BASKET,
    item
  });

export const removeFromBasket = id => (
  {
    type: REMOVE_FROM_BASKET,
    id
  });

export const setEmptyBasket = (idOrder) => (
  {
    type: SET_EMPTY_BASKET,
    idOrder
  });

export const checkBasket = () => (
  {
    type: CHECK_BASKET
  });


export default basketReducer;