const OPEN_MENU = 'OPEN_MENU';
const CLOSE_MENU = 'CLOSE_MENU';
const OPEN_BASKET = 'OPEN_BASKET';
const CLOSE_BASKET = 'CLOSE_BASKET';

let initialState = {
  activeMenu: false,
  activeBasket: false
};

const interfaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MENU:
      return {
        ...state,
        activeMenu: true,
        activeBasket: false
      };
    case CLOSE_MENU:
      return {
        ...state,
        activeMenu: false
      };
    case OPEN_BASKET:
      return {
        ...state,
        activeBasket: true,
        activeMenu: false
      };
    case CLOSE_BASKET:
      return {
        ...state,
        activeBasket: false
      };
    default:
      return state;
  }
};

export const openMenu = () => (
  {
    type: OPEN_MENU
  }
);

export const closeMenu = () => (
  {
    type: CLOSE_MENU
  }
);

export const openBasket = () => (
  {
    type: OPEN_BASKET
  }
);

export const closeBasket = () => (
  {
    type: CLOSE_BASKET
  }
);

export default interfaceReducer;