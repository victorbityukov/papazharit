import React from 'react';
import css from "./BasketList.module.css";
import {LazyLoadImage} from "react-lazy-load-image-component";
import './BasketList.css';
import {calculateDiscount} from "../../functions/functions";


const BasketList = (props) => {
  return (
    <>
      {props.basketInCheckout ? <h5>Сумма: {props.totalPrice}₽</h5> : ''}
      <div className={'basket-list '+ css['basket-list'] + ' ' + (props.addStyle ? css[props.addStyle] : '')}>
        {Object.keys(props.items).map((key) => (
          <div key={key} className={css['item']}>
            <div className={css['img-container']}>
              <LazyLoadImage className={css['image']}
                             alt={'Element'}
                             effect="opacity"
                             src={props.items[key].img_url}
              />
            </div>
            <div className={css['item-container-info']}>
              <div className={css['item-title']}>
                {props.items[key].title}
              </div>
              <div className={css['item-price']}>
                {props.items[key].count} x {calculateDiscount(props.items[key].price, props.items[key].discount)}₽
              </div>
              <div className={css["btn-container"]}>
                <div className={css['btn-count']}
                     onClick={props.removeFromBasket.bind(this, props.items[key].id)}>-
                </div>
                <div className={css['btn-count']}
                     onClick={props.addToBasket.bind(this, props.items[key])}>+
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default BasketList;