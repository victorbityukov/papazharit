import React, {Component} from 'react'
import css from './Basket.module.css'
import {connect} from "react-redux";
import {addToBasket, removeFromBasket} from "../../../redux/basket-reducer";
import {openBasket, closeBasket} from "../../../redux/interface-reducer";
import {Link} from "react-router-dom";
import {LazyLoadImage} from "react-lazy-load-image-component";
import BasketList from "./BasketList/BasketList";
import {getTotalByField} from "../functions/functions";

class Basket extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        {!this.props.active ?
          <div className={`${css['basket']} ${css[this.props.theme]}`} onClick={this.props.openBasket}>
            <div>Корзина {this.props.countItems > 0 ?
              <span className={css['count']}>{this.props.countItems}</span> : ''}
            </div>
          </div> : ''
        }
        <div
          className={this.props.active ? `${css['basket-container']} ${css['active']}` : `${css['basket-container']}`}>
          <h4 className={css['title-basket']}>Корзина</h4>
          <div className={css['close-btn']} onClick={this.props.closeBasket}>&#10006;</div>
          <BasketList {...this.props}/>
          <div className={css['total']}>
            Сумма: {this.props.totalPrice}₽
          </div>
          <Link onClick={this.props.closeBasket} to={"/checkout"}
                className={css['btn-query'] + ' ' + ((Object.keys(this.props.items).length <= 0) ? css['disable-link'] : '')}>
            Оформить заказ
          </Link>
        </div>
      </>
    );
  }
}


let mapStateToProps = ({basket, inter}) => {
  return {
    totalPrice: getTotalByField(basket.items, 'price'),
    countItems: getTotalByField(basket.items, 'count'),
    items: basket.items,
    active: inter.activeBasket,
  }
};


let mapDispatchToProps = dispatch => ({
  addToBasket: item => dispatch(addToBasket(item)),
  removeFromBasket: id => dispatch(removeFromBasket(id)),
  openBasket: () => dispatch(openBasket()),
  closeBasket: () => dispatch(closeBasket()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Basket)