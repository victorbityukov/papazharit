import React from "react";
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css'
import css from './Banners.module.css';
import {LazyLoadImage} from "react-lazy-load-image-component";


const Slide = ({children, color, background}) => {
  return (
    <div className={css.container}>
      <div className={css.content} style={
        {
          background: "url(" + background + ")",
          backgroundSize: "cover",
          backgroundPosition: "center"
        }
      }/>
    </div>
  );
};


const Banners = (props) => {
  const params = {
    slidesPerView: 1,
    autoplay: {
      delay: 10000,
      disableOnInteraction: false
    },
    effect: 'coverflow',
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: (index, className) => {
        return '<span class="' + className + ' ' + css[className] + '"><div></div></span>';
      }
    }
  };
  return (
    <div  id={'section' + props.id_section} className={css['swipe-container'] + ' head-banner'}>
      <Swiper {...params}>
        {props.items.map((item, idx) => (
          <div key={idx}>
            <Slide color={item.color} background={item.img_url}/>
          </div>
        ))}
      </Swiper>
    </div>
  );
};

export default Banners;

