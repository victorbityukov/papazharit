import React from 'react';
import css from "./Address.module.css";

const Address = (props) => {
  return (
    <div className={css['address-container']}>
      <h3 className={css['address']}>Новосибирск</h3>
      <h5 className={css['tagline']}>{/*Доставка за 60 минут или вы пообедаете бесплатно*/}</h5>
    </div>
  )
};

export default Address;