import React from "react"
import css from "./Title.module.css"
import {Link} from "react-router-dom";

const Title = (props) => {
  return (
    <>
      <Link className={css['logo']} to="/"/>
      <div className={css['title-container']}>
        <h1 className={css['title']}>
          <Link to="/">Papa<span className={css[props.theme]}>Жарит</span></Link>
        </h1>
        <h5 className={css['subtitle']}>{/*Небольшой слоган или фраза*/}</h5>
      </div>
    </>
  );
};

export default Title;