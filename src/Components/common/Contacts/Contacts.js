import React from "react"
import css from "./Contacts.module.css"

const Contacts = (props) => {
  return (
    <>
      <div className={css['contacts-container']}>
        <h1 className={css['telephone']}>8-913-932-26-66</h1>
        <h5 className={css['time']}>{/*C 10:00 до 22:00 ежедневно*/}</h5>
      </div>
    </>
  );
};

export default Contacts;