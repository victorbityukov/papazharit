import React from 'react';
import css from "./BtnOnMain.module.css";
import {Link} from "react-router-dom";

const BtnOnMain = (props) => {
  return (
    <Link {...props} className={css['btn-on-main']} to={"/"}>
      На главную
    </Link>
  );
};

export default BtnOnMain;