import {YMaps, Map, Placemark} from "react-yandex-maps";
import React from "react";
import css from './YandexMap.module.css';
import TitleSection from "../TitleSection/TitleSection";


const mapData = {
  center: [55.048704, 82.923008],
  zoom: 14,
};

const coordinate = [55.048704, 82.923008];
const YandexMap = () => (
  <>
    <TitleSection
      to="/"
      title="А ещё у нас есть бесплатная доставка по Новосибирску"/>
    <YMaps>
      <div>
        <Map className={css["map"]} defaultState={mapData}>
          <Placemark
            geometry={coordinate}
            options={{iconColor: "#ff4200"}}
          />
        </Map>
      </div>
    </YMaps>
  </>
);

export default YandexMap