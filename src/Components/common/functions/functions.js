import {isEmpty} from "lodash";
import currentHost from "../config";

const getTotalCount = (item) => {
  return item['count'];
};

const getTotalPrice = (item) => {
  if (item.discount > 0) {
    return (item.price - Math.ceil(item.price * item.discount / 100)) * item.count;
  }
  return item.price * item.count;
};

let functionsCalculatin = {
  'count': getTotalCount,
  'price': getTotalPrice,
};

export const getTotalByField = (items, field) => {
  let total = 0;
  Object.keys(items).forEach((key) => {
    total += functionsCalculatin[field](items[key]);
  });
  return total;
};

export const gerUrlImage = (img_url) => {
  if (isEmpty(img_url)) {
    return currentHost() + "/api/default_images/logo512.png";
  }
  return img_url;
};

export const calculateDiscount = (price, discount) => {
  if (discount > 0) {
    return price - Math.ceil(price * discount / 100);
  }
  return price;
};