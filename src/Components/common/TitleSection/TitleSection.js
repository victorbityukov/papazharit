import React from "react"
import css from "./TitleSection.module.css"

const TitleSection = (props) => {
  return (
    <>
      <h2 className={css['title']}>{props.title}</h2>
      <h3 className={css['subtitle']}>{props.subtitle}</h3>
      <div className={css['line-title']}/>
    </>
  );
};

export default TitleSection;