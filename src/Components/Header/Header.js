import React from 'react';
import css from "./Header.module.css";
import Address from "../common/Address/Address";
import Title from "../common/Title/Title";
import Contacts from "../common/Contacts/Contacts";

const HeaderContainer = (props) => {
  return (
    <header>
      <Header/>
    </header>
  );
};

export default HeaderContainer;

const Header = (props) => {
  return (
    <div id="header" className={css["header-container"]}>
      <div className={css['col-left']}>
        <Address/>
      </div>
      <div className={css['col-center']}>
        <Title theme='orange'/>
      </div>
      <div className={css['col-right']}>
       <Contacts/>
      </div>
    </div>
  );
};

