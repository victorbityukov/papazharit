import React, {Component} from 'react';
import css from './Menu.module.css';
import {connect} from "react-redux";
import {Link} from "react-scroll";
import Basket from "../../common/Basket/Basket";
import {closeMenu, openMenu} from "../../../redux/interface-reducer";
import * as axios from "axios";
import Preloader from "../../common/Preloader/Preloader";

const Menu = (props) => {
  return (
    <nav className={css['menu-container']}>
      {!props.active ?
        <div className={`${css['menu-btn']} ${css['open']}`} onClick={props.openMenu}>Меню</div> : ''
      }
      <div className={props.active ? `${css['menu-items']} ${css['menu-mobile']}` : `${css['menu-items']}`}>
        <div className={css['menu-btn']} onClick={props.closeMenu}>&#10006;</div>
        {props.isReady ? Object.keys(props.items).map((key, index) => (
          <Link
            activeClass={css["active"]}
            to={'section' + props.items[key]['id_section']}
            spy={true}
            smooth={true}
            key={index}
            className={`${css["menu-item"]}`}
            offset={-75}
          >
            {props.items[key]['title_in_menu']}
          </Link>
        )) : <Preloader/>}
      </div>
      <div className={css['basket-container']}>
        <Basket/>
      </div>
    </nav>
  );
};


let mapStateToProps = ({products, inter}) => {
  return {
    items: products.items,
    active: inter.activeMenu,
    isReady: products.isReady
  }
};

let mapDispatchToProps = dispatch => (
  {
    openMenu: () => dispatch(openMenu()),
    closeMenu: () => dispatch(closeMenu()),
  }
);

let MenuContainer = connect(mapStateToProps, mapDispatchToProps)(Menu);
export default MenuContainer;

