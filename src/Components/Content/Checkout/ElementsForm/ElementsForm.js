import React from "react";
import css from "./ElementsForm.module.css";


export const Input = (props) => {
  return (
    <div className={css['container-element']}>
      <div className={css['label']}>{props.label}:</div>
      <input className={css['element-form']} {...props} />
    </div>
  )
};

export const InputCheckbox = (props) => {
  return (
    <div className={css['container-element']}>
      <div className={css['label-inline']}>{props.label}: </div>
      <input className={css['element-form-inline']} {...props} />
    </div>
  )
};

export const Error = (props) => {
  return (<>
    {props.children && <div className={css['error']}>{props.children}</div>}
    </>
  );
};