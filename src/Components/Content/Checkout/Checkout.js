import React from "react";
import css from "./Checkout.module.css";
import {Redirect} from "react-router-dom";
import {addToBasket, removeFromBasket, setEmptyBasket} from "../../../redux/basket-reducer";
import {connect} from "react-redux";
import BasketList from "../../common/Basket/BasketList/BasketList";
import FormCheckout from "./FormCheckout/FormCheckout";
import {isEmpty} from "lodash";
import BtnOnMain from "../../common/BtnOnMain/BtnOnMain";
import {getTotalByField} from "../../common/functions/functions";


const Checkout = (props) => {
  window.scrollTo(0, 0);
  return (
    <>
      {isEmpty(props.items) ? <Redirect to={"/"}/> : ''}
      <BtnOnMain/>
      <div className={css['checkout-container']}>
        <div className={css['container-form']}>
        <FormCheckout {...props}/>
        </div>
        <div className={css['container-basket']}>
          <h3>Корзина</h3>
          <BasketList {...props} addStyle={'grid'}/>
        </div>
      </div>
    </>
  );
};

let mapStateToProps = ({basket, inter}) => {
  return {
    totalPrice: getTotalByField(basket.items, 'price'),
    countItems: getTotalByField(basket.items, 'count'),
    items: basket.items,
    active: inter.activeBasket,
    basketInCheckout: true,
    infoForOrder: basket.infoForOrder
  }
};

let mapDispatchToProps = dispatch => ({
  addToBasket: item => dispatch(addToBasket(item)),
  removeFromBasket: id => dispatch(removeFromBasket(id)),
  setEmptyBasket: idOrder => dispatch(setEmptyBasket(idOrder)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);