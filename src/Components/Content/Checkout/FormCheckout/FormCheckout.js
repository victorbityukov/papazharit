import React from 'react';
import css from './FormCheckout.module.css';
import {Error, Input, InputCheckbox} from "../ElementsForm/ElementsForm";
import {Formik} from "formik";
import * as axios from "axios";
import {Redirect} from "react-router-dom";
import {isEmpty} from "lodash";
import currentHost from "../../../common/config";

let getInfoAboutFromItems = (items) => {
  let dataItems = [];
  Object.keys(items).forEach((key)=>{
    let {id, count} = items[key];
    dataItems.push([id, count]);
  });
  return dataItems;
};

const FormCheckout = (props) => {
  return (
    <Formik
      initialValues={
        {
          name: '',
          email: '',
          phone: '',
          comment: '',
          street: '',
          house: '',
          flat: '',
          pickup: ''
        }
      }
      validate={values => {
        const errors = {};
        if (values.email &&
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Почта некорректна';
        }

        if (!values.email && !values.phone) {
          errors.emailOrPhone = 'Email или телефон должны быть заполнены';
        }

        if (!values.pickup) {
          if (!values.street || !values.house) {
            errors.address = 'Необходимо заполнить адрес';
          }
        }
        return errors;
      }}
      onSubmit={(values, {setSubmitting}) => {
        values['items'] = getInfoAboutFromItems(props.items);

        if (values['pickup']) {
          values['pickup'] = 1;
        }

        let formData = new FormData();
        Object.keys(values).forEach((key) => {
          formData.append(key, values[key]);
        });

        formData.append('event', 'sendOrder');

        axios({
          url: currentHost() + '/api/checkout/',
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          withCredentials: true,
          data: formData,
        }).then((res) => {
          props.setEmptyBasket(res.data.data.order);
        }, (err) => {
        });
        setSubmitting(false);
      }}
    >
      {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
        <form className={css['checkout-form']} onSubmit={handleSubmit}>
          {(!!props.infoForOrder.idOrder && isEmpty(props.items)) ? <Redirect to={"/thanks"}/> : ''}
          <h3 className={css['title-section-form']}>Контактные данные</h3>
          <Input
            type="text"
            name="name"
            label="Имя"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.name}
          />
          <div className={css['group-elements']}>
            <Input
              type="email"
              name="email"
              label="Email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            <Input
              type="text"
              name="phone"
              label="Телефон"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phone}
            />
          </div>
          <Error>{errors.email && touched.email && errors.email}</Error>
          <Error>{errors.phone && touched.phone && errors.phone}</Error>
          <Error>{errors.emailOrPhone && (touched.email || touched.phone) && errors.emailOrPhone}</Error>
          <Input
            type="text"
            name="comment"
            label="Комментарий к заказу"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.comment}
          />
          <h3 className={`${css['title-section-form']} ${css['where-title']}`}>Куда доставить?</h3>
          <InputCheckbox
            type="checkbox"
            name="pickup"
            label="Самовывоз"
            onChange={handleChange}
            onBlur={handleBlur}
            checked={values.pickup}
            value={""}
          />
          {!values.pickup ? <>
            <Input
              type="text"
              name="street"
              label="Улица"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.street}
            />
            <div className={css['group-elements']}>
              <Input
                type="text"
                name="house"
                label="Дом"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.house}
              />
              <Input
                type="text"
                name="flat"
                label="Квартира/Офис"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.flat}
              />
            </div>
            <Error>{errors.address && (touched.street || touched.house) && errors.address}</Error>
          </> : ''}
          <div className={css['container-submit']}>
            <button className={css['btn-submit']} type="submit" disabled={isSubmitting}>
              Заказать
            </button>
          </div>
        </form>
      )}
    </Formik>
  )
};

export default FormCheckout;
