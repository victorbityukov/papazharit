import React from "react";
import css from "./CheckoutThanks.module.css";
import BtnOnMain from "../../../common/BtnOnMain/BtnOnMain";
import {connect} from "react-redux";
import {setEmptyBasket} from "../../../../redux/basket-reducer";
import {isEmpty} from "lodash";
import {Redirect} from "react-router-dom";
import {calculateDiscount, getTotalByField} from "../../../common/functions/functions";
import ReactPixel from "react-facebook-pixel";

const CheckoutThanks = ({idOrder, items, totalPrice}) => {
  if (!isEmpty(idOrder)) {
    let contents = [];
    Object.keys(items).forEach((key) => {
      contents.push(
        {
          id: items[key].id,
          quantity: items[key].count
        }
      )
    });
    let dataPixel = {
      value: totalPrice,
      currency: 'RUB',
      content_type: 'product',
      contents: contents
    };
    ReactPixel.track('Purchase', dataPixel);
  }

  return (
    <>
      {isEmpty(idOrder) ? <Redirect to={"/"}/> : ''}
      <BtnOnMain/>
      <h1 className={css['thanks']}>Спасибо!</h1>
      <h3 className={css['id-order-static']}>Номер заказа:</h3>
      <h2>{idOrder}</h2>
      <ul className={css['items']}>
        {Object.keys(items).map((key) => (
          <li className={css['item']} key={items[key].id}>
            {items[key].count} x {items[key].title} — {(items[key].count * calculateDiscount(items[key].price, items[key].discount))}₽
          </li>
        ))}
      </ul>
      <h3 className={css['amount']}>Сумма: {totalPrice}₽</h3>
    </>);
};

let mapStateToProps = ({basket}) => {
  return {
    items: basket.infoForOrder.items,
    idOrder: basket.infoForOrder.idOrder,
    totalPrice: getTotalByField(basket.infoForOrder.items, 'price'),
  }
};

let mapDispatchToProps = dispatch => ({
  setEmptyBasket: () => dispatch(setEmptyBasket()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutThanks)