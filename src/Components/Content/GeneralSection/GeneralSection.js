import React from "react";
import css from "./GeneralSection.module.css";
import GeneralBlock from "./GeneralBlock/GeneralBlock";
import TitleSection from "../../common/TitleSection/TitleSection";

const GeneralSection = (section) => {
  return (
    <div id={'section' + section.id_section} className={css['general-section']}>
      <TitleSection
        title={section.title_section}
        subtitle={section.subtitle_section}/>
      <div className={css['general-content']}>
        {section.items.map((element, index) => (
          <div className={css['block-section']} key={element.id}>
            <GeneralBlock {...element}/>
          </div>
        ))}
      </div>
    </div>
  );
};

export default GeneralSection;