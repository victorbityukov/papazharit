import React from "react";
import css from "./GeneralBlock.module.css";
import {LazyLoadImage} from "react-lazy-load-image-component";
import {connect} from "react-redux";
import {addToBasket} from "../../../../redux/basket-reducer";
import "./GeneralBlock.css";
import {calculateDiscount, gerUrlImage} from "../../../common/functions/functions";

const GeneralBlock = (product) => {
  const {title, description, price, discount, weight, img_url, addToBasket, addedCount} = product;
  return (
    <div className={`${css['general-block']} general-block`}>
      <LazyLoadImage onClick={addToBasket.bind(this, {...product})} className={`${css['image']}`}
                     alt={'Element'}
                     effect="opacity"
                     src={gerUrlImage(img_url)}
      />
      <div className={css["block-info"]}>
        <h5 className={css["title-block"]}>{title} <span className={css['weight']}>{weight > 0 ? weight + 'г' : ''}</span></h5>
        <p className={css["description"]}>{description}</p>
        <div className={css["btn-container"]}>
          <p className={css["price"]}>{discount > 0 ? <><span
            className={css['new-price']}>{calculateDiscount(price, discount)}₽</span>
            <span className={css['discount']}>-{discount}%</span>
          </> : price + '₽'}</p>
          <div className={css["add-btn"]} onClick={addToBasket.bind(this, {...product})}>Добавить {addedCount > 0 ?
            <span className={css['count']}>{addedCount}</span> : ''}</div>
        </div>
      </div>
    </div>
  );
};

let mapStateToProps = ({basket}, {id}) => {
  return {
    addedCount: (basket.items[id]) ? basket.items[id].count : 0
  }
};

let mapDispatchToProps = dispatch => ({
  addToBasket: item => dispatch(addToBasket(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GeneralBlock)