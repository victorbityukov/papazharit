import React from 'react'
import css from './Feedback.module.css'
import TitleSection from "../../common/TitleSection/TitleSection";
import Swiper from 'react-id-swiper';

const Feedback = (section) => {
  const params = {
    slidesPerView: 1,
    autoplay: {
      delay: 10000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: (index, className) => {
        return '<span class="' + className + ' ' + css[className] + '"></span>';
      }
    }
  };
  return (
    <div id={'section' + section.id_section} className={css["feedback-section"]}>
      <TitleSection
        to="/"
        title={section.title_section}
        subtitle={section.subtitle_section}/>
      <div className={css["feedback-content"] + ' feedback'}>
        <Swiper {...params}>
          {section.items.map((item, idx) => (
            <div key={idx}>
              <SlideFeedback {...item} background={item.img_url}/>
            </div>
          ))}
        </Swiper>
      </div>
    </div>
  )
};


const SlideFeedback = ({background, title, description, data}) => {
  return (
    <div className={css.container + ' container-general'}>
      <div className={css.img} style={
        {
          background: "url(" + background + ")",
          backgroundSize: "cover",
          backgroundPosition: "center"
        }
      }/>
      <div className={css.data}>
        <div className={css['data-content']}>
          <p className={css['name']}>{title}</p>
          <div className={css['line-mini']}/>
          <p className={css['feedback-content-text']}>{description}</p>
          <div className={css['line-full']}/>
        </div>
      </div>
    </div>
  );
};

export default Feedback;


