import React from "react";
import css from "./HoverBlock.module.css";
import {addToBasket} from "../../../../redux/basket-reducer";
import {connect} from "react-redux";
import {calculateDiscount, gerUrlImage} from "../../../common/functions/functions";


const HoverBlock = (product) => {
  const {title, description, price, weight, discount, img_url, addToBasket, addedCount} = product;
  return (
    <div className={css['hover-block']} onClick={addToBasket.bind(this, {...product})} style={{backgroundImage: "url(" + gerUrlImage(img_url) + ")"}}>
      <div className={css["block-info-container"]}>
        <div className={css["block-info"]}>
          <h5 className={css["title-block"]}>{title}</h5>
          <div className={css["line"]}/>
          {!!description ?
            <>
              <p className={css["description"]}>{description}</p>
              <div className={css["line"]}/>
            </> : ''}
          <p className={css["price"]}>{discount > 0 ? <>
            <span className={css['discount']}>-{discount}%</span>
            <span
              className={css['new-price']}>{calculateDiscount(price, discount)}₽</span>
          </> : price + '₽'} / {weight}г</p>
          <div className={css["add-btn"]}>Добавить {addedCount > 0 ?
            <span className={css['count']}>{addedCount}</span> : ''}</div>
        </div>
      </div>
    </div>
  );
};

let mapStateToProps = ({basket}, {id}) => {
  return {
    addedCount: (basket.items[id]) ? basket.items[id].count : 0
  }
};

let mapDispatchToProps = dispatch => ({
  addToBasket: item => dispatch(addToBasket(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HoverBlock)