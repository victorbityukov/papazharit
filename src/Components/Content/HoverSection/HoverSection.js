import React from "react";
import css from "./HoverSection.module.css";
import HoverBlock from "./HoverBlock/HoverBlock";
import TitleSection from "../../common/TitleSection/TitleSection";

const HoverSection = (section) => {
  return (
    <div id={'section' + section.id_section} className={css['hover-section']}>
      <TitleSection
        title={section.title_section}
        subtitle={section.subtitle_section}/>
      <div className={css['hover-content']}>
        {section.items.map((element, index) => (
          <div className={css['block' + index]} key={element.id}>
            <HoverBlock {...element}/>
          </div>
        ))}
      </div>
    </div>
  );
};

export default HoverSection;