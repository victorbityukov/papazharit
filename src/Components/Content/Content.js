import React, {Component} from "react";
import css from "./Content.module.css";
import Banners from "../common/Banners/Banners";
import HoverSection from "./HoverSection/HoverSection";
import GeneralSection from "./GeneralSection/GeneralSection";
import Feedback from "./Feedback/Feedback";
import YandexMap from "../common/YandexMap/YandexMap";
import {connect} from "react-redux";
import MenuContainer from "../Header/Menu/Menu";
import {setProducts} from "../../redux/products-reducer";
import * as axios from "axios";
import Preloader from "../common/Preloader/Preloader";
import {Route, Switch} from "react-router-dom";
import Checkout from "./Checkout/Checkout";
import CheckoutThanks from "./Checkout/CheckoutThanks/CheckoutThanks";
import {checkBasket} from "../../redux/basket-reducer";
import currentHost from "../common/config";

class Content extends Component {
  componentDidMount() {
    axios({
      method: 'GET',
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      url: currentHost() + '/api/content?event=getAll',
      withCredentials: true,
    })
      .then(response => {
        if (response.data.resultCode === 1) {
          this.props.setProducts(response.data.data);
        }
      });
    if (!this.props.isCheckBasket) {
      this.props.checkBasket();
    }
  };

  render() {
    const {products, isReady} = this.props;
    let listSections = [];
    if (products) {
      products.forEach((product, index) => {
        switch (product['section_type_display']) {
          case 'hover':
            listSections.push(<HoverSection {...product}/>);
            break;
          case 'general':
            listSections.push(<GeneralSection {...product}/>);
            break;
          case 'feedback':
            listSections.push(<Feedback {...product}/>);
            break;
          case 'banner':
            listSections.push(<Banners {...product}/>);
            break;
        }
      });
    }

    return (
      <div className={css['main']}>
        <div className={css['background']}/>
        <Switch>
          <Route path="/checkout" component={Checkout}/>
          <Route path="/thanks" component={CheckoutThanks}/>
          <Route path="/">
            <MenuContainer/>
            {!isReady ? <Preloader/> : listSections.map((product, i) => (
              <div key={i}>
                {product}
              </div>
            ))}
            {/*<YandexMap/>*/}
          </Route>
        </Switch>
      </div>
    );
  }
}

let mapStateToProps = ({products, basket}) => {
  return {
    products: products.items,
    isReady: products.isReady,
    isCheckBasket: basket.isCheck
  }
};

let mapDispatchToProps = dispatch => ({
  setProducts: products => dispatch(setProducts(products)),
  checkBasket: () => dispatch(checkBasket())
});

let ContentContainer = connect(mapStateToProps, mapDispatchToProps)(Content);

export default ContentContainer;