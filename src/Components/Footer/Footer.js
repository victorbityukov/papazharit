import React from 'react';
import css from "./Footer.module.css"
import Title from "../common/Title/Title";
import Address from "../common/Address/Address";
import Contacts from "../common/Contacts/Contacts";

const FooterContainer = (props) => {
  return (
    <footer>
      <Footer/>
    </footer>
  );
};

export default FooterContainer;

const Footer = (props) => {
  return (
    <div className={css['footer-container']}>
      <div className={css['col-left']}>
        <Title/>
      </div>
      <div className={css['col-center']}>
        <Address/>
      </div>
      <div className={css['col-right']}>
        <Contacts theme='light'/>
      </div>
    </div>
  );
};

